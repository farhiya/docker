# Docker

## What is a container?

It's a unit of software that packages up code and all its dependancies so the application can run quicker and reliably from one computing environment to another. 

There are docker container images that includes everything needed to run an application: code, runtime, system tools and libraries and settings.

## How is it different to a VM?

container visual the system whereas the vm will.

A container can have multiple applications/projects in 1 operating system. This allows different functionalities of an application to run independantly

## How is it similar?

You can deployed multiple isolated services on a single platform. As oppose to installing sofwares on your computer and trying to them seperatly.


## What is docker?

Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers.

## What other services of Containers exist?

There are others but docker is the standard

## Main sections of docker?

Docker Swarm, Docker Compose, Docker Images, Docker Daemon, Docker Engine.



### Some Docker commands

To check what containers are running:

`docker ps`


To start container:

`docker run -d -p 80:80 docker/getting-started`

-d = detach, so it runs in the background
-p = port mapping --> <outside-container(eg.chrome)>:<inside-container>


To stop the container:
`docker stop <container-name>`
or
`docker stop <container-id>`




task

go get a image of httpd
`docker pull httpd`

how can you "ssh" into your docker container
`docker exec -it <contain-name> /bin/bash`

have a look around

Start another server with nginx on a different port

look at opiton -dit

what does that do?

How do you remove theses images form your computer?


`docker stop <container-name>`

`docker image rm --force httpd`







more commands 

```bash

`docker build .`

`docker images`

`docker build . -t opinonated-httpd`

`docker run -dit opinonated-httpd`

`docker ps`

`docker run -dit -p 900:80 --name fj-httpd opinated-httpd`    


```